package Exercise4;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class MidosWorkerThread implements Runnable
{
	// ---- Parameters ------------------------------------------------------------------------------
	
	/** m : the number of instances */
	private int m;
	/** n: the number of binary attributes */
	private int n;
	/** k: the maximum number of interesting subgroups allowed in the */
	private int k;
	/**  a parameter that indicates which quality function should be used out of the 3 given */
	private int F;
	/** the input data read from the file*/
	private List<Boolean[]> data;
	/** the solutions */
	private Vector<MidosData> solutions;
	/** sets the starting attribute index for this thread */
	private final int attributeIndex;
	/** the total number of instances from the database that give a positive result (true) */
	private double total_p;
	
	// ---- Methods ------------------------------------------------------------------------------
	/**
	 * Constructor for the main worker that computes a branch of the hypothesis tree
	 * @param attributeIndex
	 * 					: indicates the index where the first attribute was specialized from the null hypothesis
	 * @param m
	 * 					: number of instances in the database
	 * @param n
	 * 					: the number of binary attributes
	 * @param k
	 * 					: the maximum number of interesting subgroups allowed in the
	 * @param F
	 * 					: the input data read from the file
	 * @param solutions
	 * 					: the solutions
	 * @param data
	 * 					: the input data read from the file
	 */
	public MidosWorkerThread(int attributeIndex, int m, int n, int k, int F, double total_p, List<Boolean[]> data)
	{
		this.data = data;
		this.solutions = new Vector<MidosData>();
		this.attributeIndex = attributeIndex;
		this.m = m;
		this.n = n;
		this.k = k;
		this.F = F;
		this.total_p = total_p;
	}
		
	/**
	 * this is the main subroutine of the algorithm Midos using breath first
	 * traversal of H. This methods uses recursion to create a tree based 
	 * structure of the Hypothesis.
	 */
	public void run()
	{
		// skip if the class was not properly initialized
		if (data == null || data.size() == 0)
		{
			System.out.println("Did not initialize data!");
			return;
		}
		
		// initialize null hypothesis and other parameters
		Boolean[] hypothesis = new Boolean[n];
		for (int i = 0; i < n; i++) 
		{
			hypothesis[i] = null;
		}		
		
		// initiate n number of threads to compute the hypothesis
		// WARNING! this excludes the null hypothesis
		//for (int i = 0; i < n; i++) 
		//{
			final int index = attributeIndex;
			
			hypothesis[index] = false;			
			compute(data.stream().filter(x -> x[index].booleanValue() == hypothesis[index].booleanValue()).collect(Collectors.toList()),
					hypothesis, 0);
			hypothesis[index] = true;			
			compute(data.stream().filter(x -> x[index].booleanValue() == hypothesis[index].booleanValue()).collect(Collectors.toList()),
					hypothesis, 0);
			hypothesis[index] = null;
		//}
		
	}
	
	/**
	 * This method recursively computes the a tree based structure of the hypothesis space
	 * Each level indicates the number of attributes that have a variation of 1 / 0 from the 
	 * general null hypothesis. For each variation it checks if this new hypothesis provides a better 
	 * quality than the currently found ones
	 * 
	 * @param data 
	 * 				: the data containing the information read from the database
	 * @param hypothesis
	 * 				: the current hypothesis
	 * @param lastIndex
	 * 				: index of the last modified attribute of the hypothesis from the parent hypothesis
	 */
	private void compute(List<Boolean[]> data, Boolean[] hypothesis, int lastIndex)
	{
		if (data.size() == 0)
			return;
		
		if (!checkSolution(data, hypothesis))
			return;
		
		// we are modifying the attributes of the hypothesis from left to right.
		for (int i = lastIndex + 1; i < hypothesis.length; i++) {
			if (hypothesis[i] == null)
			{		
				// compute for both binary cases when we specialize an attribute to true / false
				final int index = i;
				hypothesis[i] = true;
				compute(data.stream().filter(x -> x[index].booleanValue() == hypothesis[index].booleanValue()).collect(Collectors.toList()), 
						hypothesis, i);
				hypothesis[i] = false;
				compute(data.stream().filter(x -> x[index].booleanValue() == hypothesis[index].booleanValue()).collect(Collectors.toList()), 
						hypothesis, i);
				// reset the hypothesis to null because we are passing a reference from the array
				// so that when we go back up recursively it continues with the proper hypothesis 
				hypothesis[i] = null;
			}
		}
	}
	
	/**
	 * Checks if the current hypothesis has a better quality than the previous ones.
	 * If it does then it adds it to the solutions list replacing the appropriate one
	 * 
	 * @param data
	 * 					: the input data read from the file filtered for the current hypothesis
	 * @param hypothesis
	 * 					: the current hypothesis to be checked
	 */
	private boolean checkSolution(List<Boolean[]> data, Boolean[] hypothesis) 
	{
		// check if we have any data, if not there is no need to continue
		int sizeTransactions = data.size();
		if (sizeTransactions == 0)
			return false;
		
		// data is already filtered for this item
		double g = ((double)sizeTransactions) / (double)m;		
		// get the number of positive transactions this data set has
		long sizePositiveTransactions = data.stream().filter(x -> x[n] == true).count();//x[n] != null && x[n].booleanValue() == true
		double ratio = (double) sizePositiveTransactions / sizeTransactions;
		double curQ = calculateQuality(F, g, ratio);

		if (solutions.size() > k)
		{
			// if we found a better hypothesis then we need to replace one with a worse quality
			if (solutions.firstElement().getQuality() < curQ)
				solutions.remove(0);//solutions.lastElement());			
			else
				return false;			
		}

		// this will happen when first starting the algorithm, the solutions list will be empty 
		// make a perfect copy getting rid of references
		Boolean[] newHyptothesis = new Boolean[hypothesis.length];
		for (int i = 0; i < newHyptothesis.length; i++) {
			newHyptothesis[i] = hypothesis[i] == null ? null : hypothesis[i].booleanValue();
		}
		solutions.add(new MidosData(curQ, newHyptothesis, (int) sizePositiveTransactions));
		// sort the list so that we can always remove at 0 next time
		solutions.sort(null);
		return true;
	}

	/**
	 * calculate the quality value, select the formula based on the value of F
	 * 
	 * @param F
	 * 			: used to select the desired quality function
	 * @param g
	 * @param p_hat
	 * @return
	 * 			: the quality of the current hypothesis
	 */
	public double calculateQuality(int F, double g, double p_hat) 
	{
		switch (F) {
		case 1:
			return Math.sqrt(g) * Math.abs(p_hat - total_p);
		case 2:
			return (g / (1 - g)) * Math.pow(p_hat - total_p, 2);
		case 3:
			return g * (2 * p_hat - 1) + 1 - total_p;
		default:
			return 0.0;
		}

	}
	
	/**
	 * print the desired hypothesis
	 */
	@SuppressWarnings("unused")
	private void printHypothesis(Boolean[] new_h0) {
		System.out.println();
		for (Boolean i : new_h0) {
			String x = i == null ? " " : (i == true ? "1" : "0");
			System.out.print(x + ",");
		}

	}

	/** return the solutions */
	public Vector<MidosData> getSolutions(){
		return solutions;
	}
	
}
