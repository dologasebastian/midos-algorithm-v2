package Exercise4;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MidosDriver {

	public static void main (String [] args) {
		
		MidosAlgorithm midosInstance = new MidosAlgorithm();
		double confParameter = 2.58;
		midosInstance.setConfParam(confParameter);
		MidosUtil.parseInput("SPECT.traintest.txt", midosInstance);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("output.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int [] Fs = {1, 2, 3};
		for(int F: Fs ) {
			midosInstance.clearSolution();
			midosInstance.setF(F);
			midosInstance.start();
			midosInstance.printSolution(writer);
		}	
		writer.close();
		System.out.println("Done.");
	}
}
