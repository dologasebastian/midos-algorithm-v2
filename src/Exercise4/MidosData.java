package Exercise4;

public class MidosData implements Comparable<MidosData> {
	
	private Double quality;
	private Boolean[] hypothesis;
	private int positiveTransactions;
	
	
	public MidosData() {

	}

	public MidosData(Double quality, Boolean[] hypothesis, int positiveTransactions) {
		this.quality = quality;
		this.hypothesis = hypothesis;
		this.positiveTransactions = positiveTransactions;
	}

	public Double getQuality() {
		return quality;
	}

	public void setQuality(Double quality) {
		this.quality = quality;
	}

	public Boolean[] getHypothesis() {
		return hypothesis;
	}

	public void setHypothesis(Boolean[] hypothesis) {
		this.hypothesis = hypothesis;
	}
	
	public int getPositiveTransactions() {
		return positiveTransactions;
	}

	public void setPositiveTransactions(int positiveTransactions) {
		this.positiveTransactions = positiveTransactions;
	}
	
	public String getHypothesisStr() {
		String cur_hStr = "";
		int i = 0;
		for (Boolean curVar : hypothesis) {
			if (curVar != null)
			{
				if (i > 0)
					cur_hStr+="*";
				cur_hStr += curVar == true ? "x_" + i : "x_" + i + "'";
			}
			i++;
		}
		return cur_hStr;
	}

	@Override public int compareTo(MidosData other) {
		double val = quality - other.getQuality();
        return val >= 0.0 ? 1 : -1;
	}
	
}
