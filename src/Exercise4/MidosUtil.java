package Exercise4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class MidosUtil {
	
	/**
	 * some helper class that parses the input and populates the input data and parameters in a mido algorithm
	 * instance
	 * @param filename
	 * @param midosInstance
	 */
	public static void parseInput(String filename, MidosAlgorithm midosInstance) {
		File file = new File(filename);
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String row = br.readLine();
			String[] valueList = row.split(",");
			int m = Integer.parseInt(valueList[0]);
			int n = Integer.parseInt(valueList[1]);
			int k = Integer.parseInt(valueList[2]);
			int F = Integer.parseInt(valueList[3]);
			midosInstance.setF(F);
			midosInstance.setK(k);
			midosInstance.setM(m);
			midosInstance.setN(n);

			List<Boolean[]> D = new ArrayList<Boolean[]>();
			while ((row = br.readLine()) != null) {
				Boolean[] instance = new Boolean[n+1];
				valueList = row.split(",");
				int j = 0;
				for (String curVal : valueList) {
					if (curVal.equals("1")) {
						instance[j] = true;
						
					} else {
						instance[j] = false;
					}
					++j;
				}
				D.add(instance);
			}
			midosInstance.setD(D);
			double total_p = (double)D.stream().filter(x -> x[n].booleanValue()).count()/m;
			midosInstance.setTotal_p(total_p);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
