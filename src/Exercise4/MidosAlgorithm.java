package Exercise4;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class MidosAlgorithm
{
	/** m : the number of instances */
	private int m;
	/** n: the number of binary attributes */
	private int n;
	/** k: the maximum number of interesting subgroups allowed in the */
	private int k;
	/**  a parameter that indicates which quality function should be used out of the 3 given */
	private int F;
	/** the input data read from the file*/
	private List<Boolean[]> data;
	/** the solutions */
	private Vector<MidosData> solutions;
	/** the total number of instances that give a positive result (true) */
	private double total_p;
	/** the confidence parameter */
	private double confidenceParam = 0.01;

	public MidosAlgorithm() {
		solutions = new Vector<MidosData>();
		m = 0;
		n = 0;
		k = 0;
		F = 0;
	}	
	
	/**
	 * Start the algorithm
	 */
	public void start()
	{
		// skip if the class was not properly initialized
		if (data == null || data.size() == 0)
		{
			System.out.println("Did not initialize data!");
			return;
		}
		
		if (solutions != null && solutions.size() > 0)
			clearSolution();
		
		// initialize null hypothesis and other parameters
		Boolean[] hypothesis = new Boolean[n];
		for (int i = 0; i < n; i++) 
		{
			hypothesis[i] = null;
		}		
		
		// initiate n number of threads to compute the hypothesis
		// WARNING! this excludes the null hypothesis
		List<Thread> MidosWorkers = new ArrayList<Thread>();
		List<MidosWorkerThread> Runnables = new ArrayList<MidosWorkerThread>();
		@SuppressWarnings("unused")
		int processors = Runtime.getRuntime().availableProcessors();
		for (int i = 0; i < n; i++) 
		{
			MidosWorkerThread x = new MidosWorkerThread(i, m, n, k ,F, total_p, data);
			Runnables.add(x);
			Thread worker = new Thread(x);
			MidosWorkers.add(worker);			
		}
		for (Thread worker : MidosWorkers) {
			worker.start();
		}
		while(true)
		{
			try {
				Thread.sleep(2000);
				boolean stop = true;
				for (Thread thread : MidosWorkers) {
					if (thread.isAlive())
						{stop = false; break;}
				}
				if (stop)
					break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		Vector<MidosData> allBranches = new Vector<MidosData>();
		for (MidosWorkerThread item : Runnables) {			
			@SuppressWarnings("unused")
			boolean addAll = allBranches.addAll(item.getSolutions());			
		}
		allBranches.sort(null);
		//for (int i = allBranches.size() - 1; i > allBranches.size() - k; i--) {
		for (int i = 0; i < k; i++) {
			solutions.add(allBranches.elementAt(i));
		}
		
	}

	/**
	 * clear the solutions vector
	 */
	public void clearSolution() {
		solutions.clear();
	}	

	/**
	 * print the solutions found by the algorithm: the hypothesis then the
	 * quality value of the hypothesis
	 */
	public void printSolution(PrintWriter writer) {
		writer.println("\n--- Printing out the solution for F: " + F + " ---");
		Iterator<MidosData> solutionIter = solutions.iterator();
		while (solutionIter.hasNext()) {
			MidosData current = solutionIter.next();
			writer.print("quality: ");
			writer.printf("%.4f", current.getQuality());
			String intersting = computeInteresting(current) ? "Interesting" : "Uninteresting";
			writer.print(", " + intersting + ",  ");	
			writer.print(current.getHypothesisStr());
			writer.println();
		}
	}	
	
	/**
	 * Compute for all subgroups in the output whether they are interesting in
	 * the sense that they significantly deviate from D with respect to the
	 * target attribute n+1 at a level of significance  = 0:01
	 */
	private boolean computeInteresting(MidosData curHypothesis) {
		double denominator = (total_p*(1-total_p))/n;
		double v = (curHypothesis.getPositiveTransactions() - total_p)/Math.sqrt(denominator);
		if(Math.abs(v) >= confidenceParam)
			return true;
		return false;
	}
	
	public void setM(int m) {
		this.m = m;
	}

	public void setN(int n) {
		this.n = n;
	}

	public void setK(int k) {
		this.k = k;
	}

	public void setF(int f) {
		F = f;
	}

	public void setD(List<Boolean[]> D) {
		this.data = D;
	}

	public void setConfParam(double confidenceParam) {
		this.confidenceParam = confidenceParam;
		
	}
	
	public void setTotal_p(double total_p) {
		this.total_p = total_p;
	}
	
}